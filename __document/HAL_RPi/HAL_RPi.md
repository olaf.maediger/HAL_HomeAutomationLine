<h1>HAL - RPi</h1>

<h2>Installation</h2>

<code>sudo apt update & sudo apt upgrade -y</code>
<code>sudo apt-get install mosquitto mosquitto-clients</code>

Startet <b>mosquitto</b> automatisch einen (auch nach dem Einschalten vorhandenen) Mqtt-Broker-Dienst?
Ja, RPi4 mit Homepage startet nach mosquitto-Installation automatisch den Broker-Dienst!

<h2>Test</h2>

Anmelden eines Subscribers am Broker:
<code>mosquitto_sub -h localhost -t '[topic]'</code>
Beispiel:
<code>mosquitto_sub -h localhost -t 'HAO/GetValue'</code>

Senden einer Nachricht zum Broker (und damit Weiterleitung zum angemeldeten Client):
<code>mosquitto_pub -h localhost -t '[topic]' -m '[value]'</code>
Beispiel:
<code>mosquitto_pub -h localhost -t 'HAO/GetValue' -m '123.45 mV'</code>

<img alt="HAL_RPi-60474473.png" src="media/HAL_RPi-60474473.png" width="80%" height="" >

<h3></h3>

<!--
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<h8></h8>
<b></b>
<i></i>
<code></code>
<code></code>
<code></code>
<></>
-->
