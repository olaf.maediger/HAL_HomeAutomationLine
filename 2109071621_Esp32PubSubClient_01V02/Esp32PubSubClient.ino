//
//----------------------------------------------------------------------
//  Esp32PubSubClient
//----------------------------------------------------------------------
// RPi-Commands for corresponding & visualising Mqtt-Communication:
// RPi Subscribe:
// mosquitto_sub -h localhost -t 'Esp32PubSubClient/Event'
// mosquitto_sub -h localhost -t 'Esp32PubSubClient/Command'
// RPi Publish:
// mosquitto_pub -h localhost -t 'Esp32PubSubClient/Event' -m 'IsActive'
// mosquitto_pub -h localhost -t 'Esp32PubSubClient/Command' -m 'LSO'
// mosquitto_pub -h localhost -t 'Esp32PubSubClient/Command' -m 'LSF'
//
//----------------------------------------------------------------------
//  Include
//----------------------------------------------------------------------
//!!! exact here !!!!!!!!!!
#define USE_WIFI_NINA false
//!!! exact here !!!!!!!!!!
#include <WiFiWebServer.h>
#include <PubSubClient.h>
//
//----------------------------------------------------------------------
//  Definition
//----------------------------------------------------------------------
const int PIN_LEDSYSTEM = 2;
//
const long unsigned MQTT_INTERVAL_ACTIVE = 60000L; // [ms]
//
const char* WLAN_SSID                 = "TPL1300SA7AZ";
// const char* WLAN_SSID                 = "FritzBoxSA7";
const char* WLAN_PASSWORD             = "01234567890123456789";
const char* MQTT_BROKER_IPADDRESS     = "192.168.178.53";   
const int   MQTT_BROKER_PORT          = 1883;
const char* MQTT_LOCAL_DEVICEID       = "Esp32PubSubClient";  
//
const char* MQTT_TOPIC_EVENT          = "Esp32PubSubClient/Event";
const char* MQTT_EVENT_CONNECTED      = "IsConnected";
const char* MQTT_EVENT_ACTIVE         = "IsActive";
//
const char* MQTT_TOPIC_COMMAND        = "Esp32PubSubClient/Command";
const char* MQTT_COMMAND_LEDSYSTEMON  = "LSO";
const char* MQTT_COMMAND_LEDSYSTEMOFF = "LSF";
//const char* MQTT_COMMANDMASK_SETVALUE = "STV %u";

const int MQTT_SIZE_TOPIC      = 64; // Enough space for Topic!!!
const int MQTT_SIZE_MESSAGE    = 64;
//
//----------------------------------------------------------------------
//  Forward
//----------------------------------------------------------------------
void MqttOnReceiveTopic(char* topic, byte* payload, unsigned int length);
bool MqttCommandDispatcher(void);
//
//----------------------------------------------------------------------
//  Global
//----------------------------------------------------------------------
WiFiClient   WifiClient;
PubSubClient MqttClient(MQTT_BROKER_IPADDRESS, MQTT_BROKER_PORT, MqttOnReceiveTopic, WifiClient);
//
long unsigned MillisPreset = millis();
//
char MqttTopic[MQTT_SIZE_TOPIC];
char MqttMessage[MQTT_SIZE_MESSAGE];
//
//----------------------------------------------------------------------
//  Helper
//----------------------------------------------------------------------
void SetupLedSystem(void)
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  //
  for (int I = 0; I < 10; I++)
  {
    digitalWrite(PIN_LEDSYSTEM, HIGH);
    delay(100);
    digitalWrite(PIN_LEDSYSTEM, LOW);
    delay(100);
  }
}
//
//----------------------------------------------------------------------
//  Callback
//----------------------------------------------------------------------
void MqttOnReceiveTopic(char* topic, byte* payload, unsigned int payloadsize) 
{
  if (0 < strlen(topic))
  {
    strcpy(MqttTopic, topic);
    char C = (char)0x00;
    unsigned S = min((int)payloadsize, MQTT_SIZE_MESSAGE - 1);
    for (unsigned int CI = 0; CI < S; CI++) 
    {
      MqttMessage[CI] = (char)payload[CI];
    } 
    MqttMessage[S] = 0x00;
    // debug 
    Serial.print("Mqtt: (CB)Received[");
    // debug 
    Serial.print(MqttTopic);
    // debug 
    Serial.print("][");  
    // debug 
    Serial.print(MqttMessage);
    // debug 
    Serial.println("]");
    MqttCommandDispatcher();
  }
}
//
//----------------------------------------------------------------------
//  Mqtt - Management
//----------------------------------------------------------------------
void MqttConnect()
{ 
  while (!MqttClient.connected())
  {
    Serial.print("Mqtt: Connect to ");
    Serial.println(MQTT_BROKER_IPADDRESS);    
    if (MqttClient.connect(MQTT_LOCAL_DEVICEID, "", ""))
    {
      Serial.println("Mqtt: Connected");            
      //  Subscribe: Event
      MqttClient.subscribe(MQTT_TOPIC_EVENT);
      Serial.print("Mqtt: Subscribe Topic[");
      Serial.print(MQTT_TOPIC_EVENT);
      Serial.println("]");
      //  Subscribe: Command
      MqttClient.subscribe(MQTT_TOPIC_COMMAND);
      Serial.print("Mqtt: Subscribe Topic[");
      Serial.print(MQTT_TOPIC_COMMAND);
      Serial.println("]");
      //  Publish: Connected
      MqttClient.publish(MQTT_TOPIC_EVENT, MQTT_EVENT_CONNECTED);
      Serial.print("Mqtt: Connection published[");
      Serial.print(MQTT_TOPIC_EVENT);
      Serial.print("][");
      Serial.print(MQTT_EVENT_CONNECTED);
      Serial.println("]");      
    }
    else
    {
      Serial.print("ERROR: Mqtt-Connection failed(rc=");
      Serial.print(MqttClient.state());
      Serial.println(")!");
      Serial.println("Mqtt: Retry after 5s...");
      delay(5000);
    }
  }
}
//
//----------------------------------------------------------------------
//  Mqtt - Send to Broker
//----------------------------------------------------------------------
void MqttSendEventActivePeriodic(void)
{
  if (MillisPreset <= millis())
  {
    MillisPreset = millis() + MQTT_INTERVAL_ACTIVE;
    if (!MqttClient.publish(MQTT_TOPIC_EVENT, MQTT_EVENT_ACTIVE))
    {
      Serial.println("ERROR: Publish Event Active failed!");
    }
    else
    {
      Serial.print("Mqtt: Transmit[");
      Serial.print(MQTT_TOPIC_EVENT);
      Serial.print("][");
      Serial.print(MQTT_EVENT_ACTIVE);
      Serial.println("]");
    }
  }  
}
//
//----------------------------------------------------------------------
//  Mqtt - Receive from Broker
//----------------------------------------------------------------------
bool MqttCommandDispatcher(void)
{
  Serial.print("Mqtt: CommandDispatcher[");
  Serial.print(MqttTopic);
  Serial.print("][");  
  Serial.print(MqttMessage);
  Serial.println("]");
  if (0 == strcmp(MQTT_TOPIC_EVENT, MqttTopic))
  {
    Serial.print("Mqtt: Received Event[");
    Serial.print(MqttMessage);
    Serial.println("]");
    return true;
  }
  if (0 == strcmp(MQTT_TOPIC_COMMAND, MqttTopic))
  {
    if (0 == strlen(MqttMessage))
    {
      Serial.println("ERROR: Undefined Command!");
      return false;
    }
    Serial.print("Mqtt: Received Command[");
    Serial.print(MqttMessage);
    Serial.println("]");
    if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMON, MqttMessage))
    { // debug Serial.print("!!!!!!!!!! ON");  
      digitalWrite(PIN_LEDSYSTEM, HIGH);
      return true;
    }
    if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMOFF, MqttMessage))
    { // debug Serial.print("!!!!!!!!!! OFF");  
      digitalWrite(PIN_LEDSYSTEM, LOW);
      return true;
    }
  return false;
  }
}
//
//----------------------------------------------------------------------
//  Setup
//----------------------------------------------------------------------
void setup()
{ // Open serial communications and wait for port to open:
  Serial.begin(115200);
  delay(333);
  Serial.println("\r\n\n*** Esp32PubSubClient");
  Serial.println("blink...");
  //
  SetupLedSystem(); 
  //
  Serial.println("Mqtt: Initializing");
  Serial.print("Mqtt: WebServer-Version: ");
  Serial.println(WIFI_WEBSERVER_VERSION);

  Serial.print("Mqtt: Connecting to: ");
  Serial.print(WLAN_SSID);
  Serial.print(" ");
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  while (WL_CONNECTED != WiFi.status())
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.print("\r\nMqtt: Connected to LocalIPAddress[");
  Serial.print(WiFi.localIP());
  Serial.println("]");
  //
  MqttClient.setServer(MQTT_BROKER_IPADDRESS, MQTT_BROKER_PORT);
  MqttClient.setCallback(MqttOnReceiveTopic);
}
//
//----------------------------------------------------------------------
//  Main
//----------------------------------------------------------------------
void loop() 
{
  if (!MqttClient.connected()) 
  { // -> open
    MqttConnect();
  }
  else
  { // -> execute
    MqttClient.loop();
    MqttSendEventActivePeriodic();
  }
}
