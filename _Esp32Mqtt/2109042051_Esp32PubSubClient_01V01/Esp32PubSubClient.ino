//
//!!! exact here !!!!!!!!!!
#define USE_WIFI_NINA false
//!!! exact here !!!!!!!!!!
#include <WiFiWebServer.h>
#include <PubSubClient.h>
//
//
//#define BOARD_TYPE  "ESP32"
//#define BOARD_NAME    BOARD_TYPE

const int PIN_LEDSYSTEM = 2;

int status = WL_IDLE_STATUS;     // the Wifi radio's status


char ssid[] = "TPL1300SA7AZ";
// char ssid[] = "FritzBoxSA7";
char pass[] = "01234567890123456789";


// Update these with values suitable for your network.
//const char* mqttServer = "broker.example";        // Broker address
//const char* mqttServer = "broker.emqx.io";        // Broker address
const char* mqttServer = "192.168.178.53";        // Broker address
//IPAddress mqttServer(192.168.178.53);

const char *ID        = "Esp32PubSubClient";  // Name of our device, must be unique
const char *TOPIC     = "test";               // Topic to subcribe to
const char *subTopic  = "test";               // Topic to subcribe to

void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.print("RxMessage[");
  Serial.print(topic);
  Serial.print("][");  
  for (unsigned int i = 0; i < length; i++) 
  {
    Serial.print((char)payload[i]);
  }  
  Serial.println("]");
}

WiFiClient  wifiClient;
PubSubClient client(mqttServer, 1883, callback, wifiClient);

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection to ");
    Serial.print(mqttServer);

    // Attempt to connect
    if (client.connect(ID, "try", "try"))
    {
      Serial.println("...connected");
      
      // Once connected, publish an announcement...
      String data = "Hello from MQTTClient_SSL!";// + String(BOARD_NAME);

      client.publish(TOPIC, data.c_str());

      //Serial.println("Published connection message successfully!");
      //Serial.print("Subcribed to: ");
      //Serial.println(subTopic);
      
      // ... and resubscribe
      client.subscribe(subTopic);
      // for loopback testing
      client.subscribe(TOPIC);
    }
    else
    {
      Serial.print("...failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");

      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  delay(399);
  Serial.println("*** Esp32PubSubClient");
  Serial.println("blink...");
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  for (int I = 0; I < 10; I++)
  {
    digitalWrite(PIN_LEDSYSTEM, HIGH);
    delay(100);
    digitalWrite(PIN_LEDSYSTEM, LOW);
    delay(100);
  }
  //
  Serial.println("Starting MQTTClient_Basic...");
  Serial.print("WebServer-Version: ");
  Serial.println(WIFI_WEBSERVER_VERSION);

  Serial.print("Connecting to: ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  while (WL_CONNECTED != WiFi.status())
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.print("\r\nConnected - Local IPAddress: ");
  Serial.println(WiFi.localIP());
  //
  client.setServer(mqttServer, 1883);
  client.setCallback(callback);
  // Allow the hardware to sort itself out
  delay(1500);
}

#define MQTT_PUBLISH_INTERVAL_MS       5000L

String data         = "Hello from MQTTClient_Basic"; // + String(BOARD_NAME);// + " with " + String(SHIELD_TYPE);
const char *pubData = data.c_str();

unsigned long lastMsg = 0;

void loop() 
{
  static unsigned long now;
  
  if (!client.connected()) 
  {
    reconnect();
  }

  // Sending Data
  now = millis();
  
  if (now - lastMsg > MQTT_PUBLISH_INTERVAL_MS)
  {
    lastMsg = now;

    if (!client.publish(TOPIC, pubData))
    {
      Serial.println("Message failed to send.");
    }

    Serial.print("TxMessage[");
    Serial.print(String(TOPIC));
    Serial.print("][");
    Serial.print(String(data));
    Serial.println("]");
  }
  digitalWrite(PIN_LEDSYSTEM, HIGH);
  delay(20);
  //  
  client.loop();
  //
  digitalWrite(PIN_LEDSYSTEM, LOW);
  delay(20);
}
